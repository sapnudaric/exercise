import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StringsService } from '../../service/strings.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  public divdashboard: any;
  public divPolicy: any;
  public disablemanage: boolean;
  public disableclaims: boolean;
  public disablepolicy: boolean;

  constructor(public router: Router, public string: StringsService) {
    this.divdashboard = true;
    this.divPolicy = false;
    this.disableclaims = false;
    this.disablemanage = false;
    this.disablepolicy = false;
   }

   gotoPolicies(){
    this.disableclaims = true;
    this.disablemanage = true;
    this.divdashboard = false;
    this.divPolicy = true;
   }

   gotoPolicyDetails(){
     this.router.navigate(["policies"]);
   }

  ngOnInit() {
  }

}
