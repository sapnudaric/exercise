import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StringsService {

  constructor() { }

  user: any = {
    greetings: "Good Afternoon",
    username: "Chu Ying, Rebecaa"
  }

  segments: any = {
    manage1: "Manage",
    manage2: "ILAS Funds",
    claims: "Claims",
    policy1: "Policy",
    policy2: "Servicing"
  }

  coverage = "Coverage from 4 Policies";

  policies: any = {
    life: "Life",
    critical: "Critical Illness",
    medical: "Medical & Hosp.",
    accident: "Accident"
  }
}
